from appJar import gui
from main import main



######################
#   PRESS ACTIONS
######################
def press(btn):
    if btn == "Exit":
        app.stop()
    elif btn == "Input file":
        click_open1()
    elif btn == "Run":
        click_run()     



######################
#   FUNCTIONS
######################
def click_open1():
    file = app.openBox("Box",dirName=None, fileTypes=[('Excel File','*.xlsx')] )
    app.setEntry("Archivo1", str(file))

    
def click_run():    
    # Acquiring files
    a1 = app.getEntry("Archivo1")
    a5 = app.getEntry("Archivo5")
    a6 = app.getEntry("Archivo6")
    # Checking all files has been selected
    if a1=="":
        print("File missing! Check input boxes.") 

    # Running app!
    else:
        print("Correr app!")
        if a5 == "":
            a5 = "Result.xlsx"
        if a6 == "":
            a6 = "Result_KML.kml"
        print("Output file:", a5)
        app.clearEntry("Status")
        app.setEntry("Status","Processing...")
        main(a1,a5,a6)
        app.clearEntry("Status")
        app.setEntry("Status","Done! Check output files")



######################
#   GUI SETUP
######################
app = gui("Network Capacity Planning","400x150")
app.setFont(10)
app.setSticky("ew")

# App Title
row=0
app.addLabel("titulo","Network Capacity Planning",row,0,3)
app.getLabelWidget("titulo").config(font=("Comic Sans", "12", "bold"))
app.setLabelBg("titulo", "light blue")

# Horizontal Separator
row=row+1
app.addHorizontalSeparator(row,0,3)

# First Column
app.setStretch("none")
row=row+1
app.addButton("Input file", press,row,0)
row=row+1
app.addLabel("savefile","Output file",row,0)
row=row+1
app.addLabel("savefile2","Output file",row,0)
row=row+1
app.setSticky("e")
app.addButton("Run", press,row,0)
app.setButtonBg("Run","green")

# Second Column
row=2
app.addLabel("l1","-->",row,1)
row=row+1
app.addLabel("l2","-->",row,1)
row=row+1
app.addLabel("l3","-->",row,1)
row=row+1

# Third Column
app.setStretch("both")
app.setSticky("ew")
row=2
app.addEntry("Archivo1",row,2)
app.disableEntry("Archivo1")
app.setButtonEntry("Archivo1","both")
row=row+1
app.addEntry("Archivo5",row,2)
app.setEntryDefault("Archivo5","Result.xlsx (Default)")
row=row+1
app.addEntry("Archivo6",row,2)
app.setEntryDefault("Archivo6","Result_KML.kml (Default)")
row=row+1
app.setSticky("w")
app.addButton("Exit", press,row,2)
app.setButtonBg("Exit","red")

# Status Bar
row=row+1
app.setSticky("ew")
app.addEntry("Status",row,0,3)
app.setEntry("Status","Waiting...")
app.disableEntry("Status")

app.go()
