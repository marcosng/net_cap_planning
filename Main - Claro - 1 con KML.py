import openpyxl
from igraph import *
import simplekml
import time

# Tabla de overbooking
dic_overbooking = {0: 1,
                   1: 1,
                   2: 1.13,
                   3: 1.35,
                   4: 1.38,
                   5: 1.5,
                   6: 1.69,
                   7: 1.75,
                   8: 1.89,
                   9: 1.93,
                   10: 2.05,
                   11: 2.15,
                   12: 2.25,
                   13: 2.34,
                   14: 2.52,
                   15: 2.6,
                   16: 2.67,
                   17: 2.73,
                   18: 2.79,
                   19: 2.85,
                   20: 3,
                   21: 3.05,
                   22: 3.19,
                   23: 3.23,
                   24: 3.38,
                   25: 3.41,
                   26: 3.5}

# Inicializo el grafo
grafo = Graph()

# Abro el excel para leerlo
file = openpyxl.load_workbook("Red_MW.xlsx")

# Creo las variables globales para los nombres de los sitios destino
nom_sitios_destino = []
errores = 0

#***********************************
#            FUNCIONES
#***********************************

# Funcion para crear el log de erroes
def log_to_file( file_name, log_text ):
    '''
    La funcion crea un archivo de log .txt (en caso que no exista, de lo contrario
    lo abre) y registra eventos.
    INPUT:  *file_name: nombre del archivo de log a escribir/crear
            *log_text: texto a registrar
    OUTPUT:  *archivo de log con los registros
    '''
    # Hora de registro del evento
    t = time.strftime("%H:%M:%S")

    # Existe el archivo de log? Lo abro y escribo. De lo contrario lo creo.
    try:
        log_file = open(file_name, 'a')
    except FileNotFoundError:
        log_file = open(file_name, 'w')
        log_file.write(t + " Archivo de log creado.")

    # Texto del evento
    log_file.write(t +" "+ log_text + '\n' )

    # Cierro el archivo
    log_file.close()
    return

# Leer sitios del excel y cargar en igraph
def cargar_sitios_en_igraph():
    global nom_sitios_destino
    global errores
    sheet = file.get_sheet_by_name("Sitios_con_enlaces")

    for i in range(2, sheet.max_row + 1):
        try:
            grafo.add_vertex(sheet.cell(row=i, column=1).value,
                         capacidad=sheet.cell(row=i, column=7).value,
                         cell_id_destino=sheet.cell(row=i, column=5).value,
                         coord_x=sheet.cell(row=i, column=3).value,
                         coord_y=sheet.cell(row=i, column=4).value,
                         camino_mas_corto=[],
                         tx_path=sheet.cell(row=i, column=6).value)
            nom_sitios_destino.append(sheet.cell(row=i,column=5).value)  # guardo los nombres de los sitios destinos para después cambiarle la forma a esos nodos que son sitios destiino
        except:
            log_to_file("Log_errores.txt", "Ocurrió un problema al cargar el sitio de la fila " + str(i))
            errores = errores + 1
    nom_sitios_destino = set(nom_sitios_destino)  # elimino duplicados

# Leer enlaces del excel y cargar en igraph
def cargar_enlaces_en_igraph():
    global errores
    sheet = file.get_sheet_by_name("Enlaces")
    for i in range(2, sheet.max_row + 1):
        try:
            grafo.add_edge(sheet.cell(row=i, column=1).value, sheet.cell(row=i, column=2).value,
                       sitio_a=sheet.cell(row=i, column=1).value,
                       sitio_b=sheet.cell(row=i, column=2).value,
                       concatenado=sheet.cell(row=i, column=1).value + sheet.cell(row=i, column=2).value,
                       capacidad_requerida=0, nro_enlaces_overbooking=0, factor_overbooking=0,
                       capacidad_requerida_overbooking=0, capacidad_instalada=sheet.cell(row=i, column=3).value)
        except:
            log_to_file("Log_errores.txt", "Ocurrió un problema al cargar el enlace de la fila " + str(i))
            errores = errores + 1

# Determinar los caminos más cortos a los sitios destino, calcular la capacidad requerida y contar los saltos
def caminos_mas_cortos_a_sitios_destino():
    global errores
    for i in grafo.vs:
        camino_mas_corto = grafo.get_shortest_paths(i['name'], i['cell_id_destino'], output='epath')
        # esta variable debería ser un array de arrays porque puede dar más de un camino más corto
        # esto quiere decir que hay más de una capacidad requerida para cada enlace dependiendo de cuál sea el camino activo
        # por eso la capaciadad requerida debería ser un array, y habría que agregar el atributo camino a cada enlace, ya que dependiendo del
        # camino por el que vaya es la capacidad que requeiere. Sin embargo, para este código se siguió no se tomó en cuenta esto.

        # Indico en un log qué sitios no llegan a los destino
        if camino_mas_corto == [[]]:
            if i['name'] != i['cell_id_destino']:
                log_to_file("Log_errores.txt", "El sitio " + i['name'] +" no puede llegar al sitio destino " + i['cell_id_destino'] + " con la red cargada.")
                errores = errores + 1
        i['camino_mas_corto'] = camino_mas_corto
        for j in camino_mas_corto[0]:
            temp = grafo.es[j]['capacidad_requerida']
            temp = temp + i['capacidad']
            grafo.es[j]['capacidad_requerida'] = temp
            grafo.es[j]['nro_enlaces_overbooking'] = grafo.es[j]['nro_enlaces_overbooking'] + 1

# Calculo de las capacidades requeridas con overbooking
def calculo_capacidad_requerida_por_overbooking():
    print('Capacidades requeridas total y con overbooking\n')
    for i in grafo.es:
        # calculo las capacidades con el overbooking incluido
        if i['nro_enlaces_overbooking'] >= 26:  # cuando hay más de 26 enlaces el factor siempre es 3.5
            i['factor_overbooking'] = 3.5
            factor = i['factor_overbooking']
        else:
            i['factor_overbooking'] = dic_overbooking[i['nro_enlaces_overbooking']]
            factor = i['factor_overbooking']
        capacidad_requerida_overbooking = i['capacidad_requerida'] / factor
        i['capacidad_requerida_overbooking'] = capacidad_requerida_overbooking
        print(i['concatenado'])
        print('Total: ' + str(i['capacidad_requerida']) + ' Mbps')
        print(
            'Overbooking: ' + str(i['capacidad_requerida_overbooking']) + ' Mbps' + ' | Nro sitios overbooking ' + str(
                i['nro_enlaces_overbooking']))
        print('\n')

# Guardo los resultados en un excel nuevo
def guardo_resultados_en_nuevo_archivo_excel():
    wb = openpyxl.Workbook()
    dest_filename = 'Result.xlsx'
    ws1 = wb.active
    ws1.title = "MW required capacity"
    ws1.cell(row=1, column=1).value = "Sitio A"
    ws1.cell(row=1, column=2).value = "Sitio B"
    ws1.cell(row=1, column=3).value = "Concatenado 1"
    ws1.cell(row=1, column=4).value = "Concatenado 2"
    ws1.cell(row=1, column=5).value = "Capacidad aritmética"
    ws1.cell(row=1, column=6).value = "N° de sitios overbooking"
    ws1.cell(row=1, column=7).value = "Capacidad con overbooking"
    ws1.cell(row=1, column=9).value = "Capacidad instalada"
    N = 2
    for i in range(len(grafo.es)):
        ws1.cell(row=N, column=1).value = grafo.es[i]['sitio_a']
        ws1.cell(row=N, column=2).value = grafo.es[i]['sitio_b']
        ws1.cell(row=N, column=3).value = grafo.es[i]['concatenado']
        ws1.cell(row=N, column=4).value = grafo.es[i]['sitio_b'] + grafo.es[i]['sitio_a']
        ws1.cell(row=N, column=5).value = grafo.es[i]['capacidad_requerida']
        ws1.cell(row=N, column=6).value = grafo.es[i]['nro_enlaces_overbooking']
        ws1.cell(row=N, column=7).value = grafo.es[i]['capacidad_requerida_overbooking']
        ws1.cell(row=N, column=9).value = grafo.es[i]['capacidad_instalada']
        N = N + 1
    wb.save(dest_filename)

# Creo los plots en .pdf y los guardo
def ploteo_grafos():
    global errores
    grafo.vs["label"] = grafo.vs["name"]
    grafo.vs['label_dist'] = 2
    grafo.vs['label_angle'] = 0

    for i in range(len(grafo.es)):
        grafo.es[i]['label'] = str(round(grafo.es[i]['capacidad_requerida_overbooking'], 2)) + ' Mbps'
        if grafo.es[i]['capacidad_requerida_overbooking'] > float(grafo.es[i]['capacidad_instalada']) * 0.65:
            grafo.es[i]['color'] = 'orange'
        if grafo.es[i]['capacidad_requerida_overbooking'] > float(grafo.es[i]['capacidad_instalada']):
            grafo.es[i]['color'] = 'red'

    for i in range(len(grafo.es)):
        grafo.es[i]['width'] = grafo.es[i]['capacidad_requerida_overbooking'] / 10

    for i in range(len(grafo.vs)):
        if grafo.vs[i]['name'] in nom_sitios_destino:
            grafo.vs[i]['shape'] = "rectangle"
        else:
            grafo.vs[i]['color'] = "gray"

    try:
        plot(grafo, "Diagrama_árbol.pdf", layout='tree', margin=100, bbox=(10000, 10000))
    except:
        log_to_file("Log_errores.txt", "Ocurrió un problema al crear el plot de árbol.")
        errores = errores + 1

    try:
        plot(grafo, "Diagrama_normal.pdf", margin=100, bbox=(5500, 5500))
    except:
        log_to_file("Log_errores.txt", "Ocurrió un problema al crear el plot normal.")
        errores = errores + 1

    array_coord = []
    for i in range(len(grafo.vs)):
        array_coord.append((grafo.vs[i]['coord_x'], grafo.vs[i]['coord_y']))
    layout_coord = array_coord

    try:
        plot(grafo, "Diagrama_coordenadas.pdf", layout=layout_coord, margin=100, bbox=(10000, 10000))
    except:
        log_to_file("Log_errores.txt", "Ocurrió un problema al crear el plot de coordenadas.")
        errores = errores + 1

# Creo el archivo KML y lo guardo
def creo_kml():
    kml = simplekml.Kml()

    # Cargo los sitios
    for i in range(len(grafo.vs)):
        pnt = kml.newpoint(name=str(grafo.vs[i]['name']),
                           coords=[(grafo.vs[i]['coord_x'],
                                    grafo.vs[i]['coord_y'])])
        pnt.description = "Capacidad requerida: " + str(grafo.vs[i]['capacidad']) + ' Mbps'
        pnt.style.iconstyle.icon.href = 'http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png'
        pnt.style.iconstyle.scale = 1.2

    # No quiero que la línea más gorda me quede con más de 10pixeles, tengo que iterar y ver cuál es la más grande
    grosor_maximo = 0
    for i in range(len(grafo.es)):
        temp = grafo.es[i]['capacidad_requerida_overbooking']
        if temp > grosor_maximo:
            grosor_maximo = temp

    # Cargo los enlaces
    for i in range(len(grafo.es)):
        # Saco las coordenadas de los extremos del enlace
        vertice_1 = grafo.vs.find(name=grafo.es[i]['sitio_a'])
        vertice_2 = grafo.vs.find(name=grafo.es[i]['sitio_b'])
        ln = kml.newlinestring(name=str(grafo.es[i]['concatenado']),
                               coords=[(vertice_1['coord_x'], vertice_1['coord_y']),
                                       (vertice_2['coord_x'], vertice_2['coord_y'])])

        ln.description = "Capacidad requerida (overbooking): " + str(
            round(float(grafo.es[i]['capacidad_requerida_overbooking']), 2)) + ' Mbps\n' + \
                         'Capacidad instalada: ' + str(
            round(float(grafo.es[i]['capacidad_instalada']), 2)) + ' Mbps\n' + \
                         'Cantidad de sitios overbooking: ' + str(
            round(float(grafo.es[i]['nro_enlaces_overbooking']), 2))
        if float(grafo.es[i]['capacidad_requerida_overbooking']) > float(grafo.es[i]['capacidad_instalada']) * 0.65:
            ln.style.linestyle.color = simplekml.Color.orange
        if float(grafo.es[i]['capacidad_requerida_overbooking']) > float(grafo.es[i]['capacidad_instalada']):
            ln.style.linestyle.color = simplekml.Color.red

        ln.style.linestyle.width = float(grafo.es[i]['capacidad_requerida_overbooking']) * 10 / grosor_maximo

    kml.save("Red_MW.kml")

#***********************************
#            PROGRAMA
#***********************************

cargar_sitios_en_igraph()
cargar_enlaces_en_igraph()
caminos_mas_cortos_a_sitios_destino()
calculo_capacidad_requerida_por_overbooking()
guardo_resultados_en_nuevo_archivo_excel()
ploteo_grafos()
creo_kml()

if errores != 0:
    print("Revise el archivo Log_errores.txt, se presentaron errores al correr el programa.")
