# README #

El programa principal es el del archivo "Main - Claro - 1 con KML.py".
Para correrlo se necesita además utilzar la planilla de Excel "Red_MW.xlsx"

# ¿QUÉ HACE EL PROGRAMA?"

El objetivo del programa es dimensionar las capacidades de los enlaces de
MW de una red para que ésta no congestione. Para esto utiliza como input
un archivo Excel "Red_MW.xlsx" en el que se cargan los enlaces y las necesidades
de tráfico de cada sitio. 

Como output el programa devuelve:

1)Un archivo Excel "Result.xlxs" en el cual se indican las capacidades 
necesarias para cada enlace para que no congestione.

2)Dos diagramas en PDF en el que se representa la red con colores que
indican que enlaces estarían congesitonados o próximos a estar
congestionados dadas las capacidades actuales de los enlaces.

3)Un archivo KML con la misma información que los diagramas en PDF pero
que se puede abrir en Google Earth.


# CARGA DE LA RED DE MW EN LA PLANILLA "RED_MW.XLSX"

En la pestaña "Sitios_con_enlaces" se deben ingresar todos los sitios que están
unidos por MW en la red.

Columna A: Sitio
Se debe indicar el identificador único de cada sito.

Columna B: Nombre
Nombre descriptivo del sitio.

Columna C, D: Longitud, Latitud

Columna E: Sitio Destino de Agregación MPLS
Se debe indicar el identificador del sitio al que se va a volcar el tráfico del
sitio que estamos ingresando. Este sitio destino debe ser alcanzado con la red 
de MW; si se llega a través de fibra óptica se debe ingresar, en vez de éste, el
último sitio con router MPLS al que se llegue con MW.

Columna F: Tx Path
Vacío

Columna G: Capacidad Pico
Se indica la capacidad en Mbps que cursará el sitio por la red de MW hasta su
sitio de destino MPLS. En general se considera el tráfico máximo medidio para
ese sitio.

En la pestaña "Enlaces" se deben ingresar todos los enlaces de MW que pertenezcan
a la red que se quiere dimensionar.

Columna A: PuntoA
Identificador único de un extremo del enlace

Columna B: PuntoB
Identificador único del otro extremo del enlace

Columna C: Capacidad configurada 
Capacidad actualmente instalada en el enlace en Mbps

Nota: es importante que no se incluyan enlaces repetidos; el programa correrá igual
pero arrojará resultados espúreos. Para corregir esto, si hay más de un enlace entre dos
sitios, se debe incluír uno solo y sumar sus capacidades instaladas.